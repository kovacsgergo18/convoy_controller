#!/usr/bin/env python

import socket
import json
import argparse
import threading
import curses

import rospy

from convoy_common.msg import ControlCommand

from car import CarAgent, VirtualObject
from fixture import car_agents


class Controller():
    def __init__(self, v_max, d_ref, car_id):
        self.v_max = v_max
        self.d_ref = d_ref
        self.car_id = car_id
        self.car_agents = car_agents
        self.virtual_objects = [
            VirtualObject(initial_speed=0.1),
            VirtualObject(position=0.3, initial_speed=0.1)
        ]
        self.markers = []
        self.UDP_IP = '0.0.0.0'
        self.UDP_PORT = 5006
        self.track_size = 6.71
        self.convoy_control_topic_name = "convoy_car_{}_convoy".format(self.car_id)

    def run(self):
        t1 = threading.Thread(target=self.process_input)
        t1.start()
        t2 = threading.Thread(target=self.get_marker_list)
        t2.start()
        rospy.init_node('convoy_controller', anonymous=True)
        rate = rospy.Rate(2)

        while not rospy.is_shutdown():
            self.control_loop()
            rate.sleep()
        t1.join()
        t2.join()

    def subscribe_car_agent(self, message):
        self.car_agents.append(
            CarAgent(
                id=message.car_id,
                control_topic_name=message.control_topic_name,
                status_topic_name=message.status_topic_name
            )
        )
        print('subscribed: {}'.format(message.car_id))

    def unsubscribe_car_agent(self, message):
        self.car_agents = [agent for agent in self.car_agents if agent.id != message.car_id]
        print('unsubscribed: {}'.format(message.car_id))
        print(self.car_agents)

    def get_marker_list(self):
        sock = socket.socket(socket.AF_INET,
                             socket.SOCK_DGRAM)
        sock.bind((self.UDP_IP, self.UDP_PORT))

        while not rospy.is_shutdown():
            data, addr = sock.recvfrom(1500)
            try:
                d = json.loads(data)
                self.markers = d['Markers']
            except json.JSONDecodeError:
                pass

    def control_loop(self):
        for car_agent in car_agents:
            marker = self.get_marker_by_id(car_agent.id)
            if marker:
                d_tcl = self.get_closest_virtual_object_distance(marker)
            else:
                d_tcl = 0.4
            cntrl_msg = ControlCommand(v_max=self.v_max, d_ref=self.d_ref, d_tcl=d_tcl)
            car_agent.send_control_command(cntrl_msg)

    def get_marker_by_id(self, id):
        for marker in self.markers:
            if marker['id'] == id:
                return marker

    def get_closest_virtual_object_distance(self, marker):
        vo_positions = []
        for vo in self.virtual_objects:
            vo_pos = vo.position
            if vo_pos > marker['t']:
                vo_positions.append(vo_pos)
            else:
                vo_positions.append(1.0+vo_pos)
        min_vo_position = min(vo_positions)
        return (min_vo_position - marker['t']) * self.track_size


    def process_input(self):
        self.control_pub = rospy.Publisher(self.convoy_control_topic_name, ControlCommand, queue_size=10)
        stdscr = curses.initscr()
        curses.cbreak()
        stdscr.keypad(1)

        stdscr.addstr(0,0,"Press + ro increase v_max, Press - to decrease v_max.")
        stdscr.addstr(1,0,"Press * ro increase d_ref, Press / to decrease d_ref.")
        stdscr.addstr(2,0,"Press q to quit.")
        stdscr.addstr(5,0,"v_max: {}".format(self.v_max))
        stdscr.addstr(6,0,"d_ref: {}".format(self.d_ref))
        stdscr.refresh()

        key = stdscr.getch()
        while key != ord('q'):
            if key == ord('+'):
                self.v_max += 0.1
            elif key == ord('-') and self.v_max >= 0.1:
                self.v_max -= 0.1
            elif key == ord('*'):
                self.d_ref += 0.1
            elif key == ord('/') and self.d_ref >= 0.1:
                self.d_ref -= 0.1
            control_msg = ControlCommand(
                v_max = self.v_max
                #d_ref = self.d_ref
            )
            self.control_pub.publish(control_msg)
            stdscr.clear()
            stdscr.addstr(0,0,"Press + ro increase v_max, Press - to decrease v_max.")
            stdscr.addstr(1,0,"Press * ro increase d_ref, Press / to decrease d_ref.")
            stdscr.addstr(2,0,"Press q to quit.")
            stdscr.addstr(5,0,"v_max: {}".format(self.v_max))
            stdscr.addstr(6,0,"d_ref: {}".format(self.d_ref))
            stdscr.refresh()
            key = stdscr.getch()
        curses.endwin()
        rospy.signal_shutdown('Exiting..')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Common car controller.')
    parser.add_argument('-v', '--v_max', help='Maximal speed.', type=float, default=0.0)
    parser.add_argument('-d', '--d_ref', help='Maximal distance between cars.', type=float, default=0.5)
    args = parser.parse_args()
    try:
        controller = Controller(args.v_max, args.d_ref, 1)
        controller.run()
    except rospy.ROSInterruptException:
        pass
    except KeyboardInterrupt:
        print('exiting on keyboard interrupt')
