# -*- encoding: utf-8 -*-
#!/usr/bin/env python

import curses


class IndividualControllerCursesView():
    def __init__(self, enable_keypad=True, cbreak_mode=True, halfdelay=10):
        self.stdscr = curses.initscr()
        curses.halfdelay(halfdelay)
        if cbreak_mode:
            curses.cbreak()
        self.stdscr.keypad(1 if enable_keypad else 0)

    def refresh_view(self, control, status, mode):
        self.stdscr.clear()
        self.stdscr.addstr(0, 0, "Press + ro increase v_max, Press - to decrease v_max.")
        self.stdscr.addstr(1, 0, "Press * ro increase d_ref, Press / to decrease d_ref.")
        self.stdscr.addstr(2, 0, "Press i to set individual control mode.")
        self.stdscr.addstr(3, 0, "Press c to set common control mode.")
        self.stdscr.addstr(4, 0, "Press q to quit.")
        self.stdscr.addstr(5, 0, "----------------")
        self.stdscr.addstr(7, 0, "v_max: {}".format(control.v_max))
        self.stdscr.addstr(8, 0, "d_ref: {}".format(control.d_ref))
        self.stdscr.addstr(9, 0, "mode: {}".format(mode))
        self.stdscr.refresh()
