# -*- encoding: utf-8 -*-
#!/usr/bin/env python

import argparse
import curses
import threading

import rospy

from convoy_common.msg import ManagementCommand, ControlCommand, Status
from car import Car
from views import IndividualControllerCursesView


class Controller():
    def __init__(self, car_id, mode, v_max, d_ref):
        self.v_max = v_max
        self.d_ref = d_ref
        self.car_id = car_id
        self.set_mode = mode
        self.mode = mode
        self.cntrl_msg = ControlCommand()
        self.latest_status = Status()
        self.car = Car()
        self.view = IndividualControllerCursesView()

    def run(self):
        t1 = threading.Thread(target=self.process_input)
        t1.start()
        mgmt_pub = rospy.Publisher('convoy_car_{}_mgmt'.format(self.car_id), ManagementCommand, queue_size=10)
        cntrl_pub = rospy.Publisher('convoy_car_{}_teleop'.format(self.car_id), ControlCommand, queue_size=10)
        rospy.Subscriber("convoy_car_{}_status".format(self.car_id), Status, self.status_callback)

        rospy.init_node('convoy_controller', anonymous=True)

        rate = rospy.Rate(2)

        while not rospy.is_shutdown():
            if self.set_mode:
                msg = ManagementCommand(mode=self.set_mode)
                mgmt_pub.publish(msg)
                self.mode = self.set_mode
                self.set_mode = ''
            self.cntrl_msg = ControlCommand(v_max=self.v_max, d_ref=self.d_ref, d_tcl=0.5)
            cntrl_pub.publish(self.cntrl_msg)
            rate.sleep()

        t1.join()

    def process_input(self):
        self.cntrl_msg = ControlCommand(v_max=self.v_max, d_ref=self.d_ref, d_tcl=0.5)
        self.view.refresh_view(self.cntrl_msg, self.latest_status, self.mode)

        key = self.view.stdscr.getch()
        while key != ord('q'):
            if key == ord('+'):
                self.v_max += 0.1
            elif key == ord('-') and self.v_max >= 0.1:
                self.v_max -= 0.1
            elif key == ord('*'):
                self.d_ref += 0.1
            elif key == ord('/') and self.d_ref >= 0.1:
                self.d_ref -= 0.1
            elif key == ord('c'):
                self.set_mode = 'convoy'
            elif key == ord('i'):
                self.set_mode = 'teleop'
            self.cntrl_msg = ControlCommand(v_max=self.v_max, d_ref=self.d_ref, d_tcl=0.5)
            self.view.refresh_view(self.cntrl_msg, self.latest_status, self.mode)
            key = self.view.stdscr.getch()
        curses.endwin()
        rospy.signal_shutdown('Exiting..')

    def status_callback(self, message):
        self.latest_status = message


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Individual car controller.')
    parser.add_argument('-c', '--car_id', help='Car id.', type=int)
    parser.add_argument('-v', '--v_max', help='Maximal speed.', type=float, default=0.0)
    parser.add_argument('-m', '--mode', help='Mode', type=str, default='teleop')
    parser.add_argument('-d', '--d_ref', help='Maximal distance between cars.', type=float, default=0.5)
    args = parser.parse_args()
    try:
        controller = Controller(args.car_id, args.mode, args.v_max, args.d_ref)
        controller.run()
    except rospy.ROSInterruptException:
        pass
