# -*- encoding: utf-8 -*-
#!/usr/bin/env python

import time
import rospy
from convoy_common.msg import Status, ControlCommand, ManagementCommand


class CarAgent():
    def __init__(self, id, control_topic_name, management_topic_name, status_topic_name):
        self.id = id
        self.control_topic_name = control_topic_name
        self.management_topic_name = management_topic_name
        self.status_topic_name = status_topic_name

        self.control_publisher = rospy.Publisher(control_topic_name, ControlCommand, queue_size=10)
        if management_topic_name:
            self.management_publisher = rospy.Publisher(management_topic_name, ManagementCommand, queue_size=10)
        self.status_subscriber = rospy.Subscriber(status_topic_name, Status, self.status_callback)

        self.status = Status()

    def send_control_command(self, command):
        self.control_publisher.publish(command)

    def send_management_command(self, command):
        self.management_publisher.publish(command)

    def status_callback(self, message):
        self.status = message


class Car():
    speed = 0.0
    acceleration = 0.0
    distance = 0.0
    battery = 0.0
    mode = ''
    hw_communication_adapter = ''


class VirtualObject():
    def __init__(self, position=0.0, timestamp=None, id=0, initial_speed=0.0):
        if timestamp is None:
            self._timestamp = time.time()
        else:
            self._timestamp = timestamp
        self._position = position
        self._speed = initial_speed
        self.id = id

    @property
    def position(self):
        now = time.time()
        self._position = (self._position + self._speed * (now - self._timestamp)) % 1
        self._timestamp = now
        return self._position

    def set_speed(self, speed):
        pos = self.position
        self._speed = speed
        return pos
